function calcularSalario (event) {
  event.preventDefault()
  const form = document.getElementById('form')
  const lbl = document.getElementById('lbl-required')
  lbl.style.display = 'none'

  if (!form.salario.value) {
    lbl.style.display = 'block'

    return false
  }

  const salarioINSS = calculoINSS(form.salario.value)
  const salarioIRPF = calculoIRPF(form.salario.value)
  const total = form.salario.value - salarioINSS - salarioIRPF

  document.getElementById('irpf').innerText = new Intl.NumberFormat('pt-BR', { style: 'currency', currency: 'BRL' }).format(salarioIRPF)
  document.getElementById('inss').innerText = new Intl.NumberFormat('pt-BR', { style: 'currency', currency: 'BRL' }).format(salarioINSS)
  document.getElementById('total').innerText = new Intl.NumberFormat('pt-BR', { style: 'currency', currency: 'BRL' }).format(total)
}

function calculoINSS (salario) {
  const SALARIO_MINIMO = 1045
  let desconto = 0

  if (salario <= SALARIO_MINIMO) {
    desconto = salario * (7.5 / 100)
  } else if (salario <= 2089.60) {
    desconto = salario * (9 / 100)
  } else if (salario <= 3134.40) {
    desconto = salario * (12 / 100)
  } else if (salario <= 6101.06) {
    desconto = salario * (14 / 100)
  }

  return desconto
}

function calculoIRPF (salario) {
  let desconto = 0

  if (salario >= 1903.99) {
    if (salario <= 2826.65) {
      desconto = salario * (7.5 / 100)
    } else if (salario <= 3751.05) {
      desconto = salario * (15 / 100)
    } else if (salario <= 4664.68) {
      desconto = salario * (11 / 100)
    } else if (salario >= 4664.68) {
      desconto = salario * (27.5 / 100)
    }
  }

  return desconto
}